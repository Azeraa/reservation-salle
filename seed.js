const mongoose = require('mongoose');
const connectDB = require('./config/db');
const User = require('./models/user');
const Salle = require('./models/salle');
const Reservation = require('./models/reservation');

const seedDB = async () => {
    await connectDB();

    // Supprimer les données existantes
    await User.deleteMany({});
    await Salle.deleteMany({});
    await Reservation.deleteMany({});

    // Ajouter des utilisateurs
    const user1 = new User({ firstname: 'Marwen', lastname:'Aydi', email: 'marwen@example.com', password: 'password123' });
    const user2 = new User({ firstname: 'Test', lastname:'Test', email: 'test@example.com', password: 'password123' });

    await user1.save();
    await user2.save();

    // Ajouter des salles
    const salle1 = new Salle({ name: 'Salle A', capacity: 10, equipment: ['Projecteur', 'Tableau Blanc'], availability: [] });
    const salle2 = new Salle({ name: 'Salle B', capacity: 20, equipment: ['TV', 'Tableau Blanc'], availability: [] });

    await salle1.save();
    await salle2.save();

    // Ajouter des réservations
    const reservation1 = new Reservation({ user: user1._id, salle: salle1._id, date: '2024-06-10', timeslot: '09:00-10:00' });
    const reservation2 = new Reservation({ user: user2._id, salle: salle2._id, date: '2024-06-10', timeslot: '10:00-11:00' });

    await reservation1.save();
    await reservation2.save();

    console.log('Base de données initialisée avec succès');
    process.exit();
};

seedDB();
