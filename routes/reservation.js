const express = require('express');
const router = express.Router();
const { createReservation, getUserReservations, updateReservation, deleteReservation } = require('../controllers/reservationController');
const auth = require('../middleware/authMiddleware');

router.post('/', auth, createReservation);
router.get('/', auth, getUserReservations);
router.put('/:id', auth, updateReservation);
router.delete('/:id', auth, deleteReservation);

module.exports = router;