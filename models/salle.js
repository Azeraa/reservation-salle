const mongoose = require('mongoose');

const ReservationSchema = new mongoose.Schema({
    start_time: { type: Date, required: true },
    end_time: { type: Date, required: true },
    reservation_id: { type: mongoose.Schema.Types.ObjectId, ref: 'Reservation', required: true }
});

const SalleSchema = new mongoose.Schema({
    name: { type: String, required: true },
    capacity: { type: Number, required: true },
    equipment: [String],
    reservations: [ReservationSchema]
});

module.exports = mongoose.model('Salle', SalleSchema);
