const Salle = require('../models/salle');

exports.getAllSalles = async (req, res) => {
    try {
        const salles = await Salle.find();
        res.json(salles);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
};

exports.createSalle = async (req, res) => {
    const { name, capacity, equipment } = req.body;
    try {
        const newSalle = new Salle({ name, capacity, equipment });
        await newSalle.save();
        res.status(201).json(newSalle);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
};

exports.updateSalle = async (req, res) => {
    const { name, capacity, equipment } = req.body;
    try {
        let salle = await Salle.findById(req.params.id);
        if (!salle) return res.status(404).json({ msg: 'Salle not found' });

        salle.name = name || salle.name;
        salle.capacity = capacity || salle.capacity;
        salle.equipment = equipment || salle.equipment;

        await salle.save();
        res.json(salle);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
};

exports.deleteSalle = async (req, res) => {
    try {
        let salle = await Salle.findById(req.params.id);
        if (!salle) return res.status(404).json({ msg: 'Salle not found' });

        await salle.remove();
        res.json({ msg: 'Salle removed' });
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
};
