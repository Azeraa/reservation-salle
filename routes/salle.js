const express = require('express');
const router = express.Router();
const { getAllSalles, createSalle, updateSalle, deleteSalle } = require('../controllers/salleController');
const auth = require('../middleware/authMiddleware');

router.get('/', getAllSalles);
router.post('/', auth, createSalle);
router.put('/:id', auth, updateSalle);
router.delete('/:id', auth, deleteSalle);

module.exports = router;