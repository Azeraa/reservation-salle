const mongoose = require('mongoose');

const ReservationSchema = new mongoose.Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    salle: { type: mongoose.Schema.Types.ObjectId, ref: 'Salle', required: true },
    start_time: { type: Date, required: true },
    end_time: { type: Date, required: true }
});

module.exports = mongoose.model('Reservation', ReservationSchema);
