const mongoose = require('mongoose');
const Reservation = require('../models/reservation');
const Salle = require('../models/salle');
const moment = require('moment');

exports.createReservation = async (req, res) => {
    const { salle, start_time, end_time } = req.body;

    try {
        const salleDoc = await Salle.findById(salle);

        const isAvailable = salleDoc.reservations.every(
            (res) => (moment(res.end_time).isSameOrBefore(moment(start_time)) ||
                      moment(res.start_time).isSameOrAfter(moment(end_time)))
        );

        if (!isAvailable) {
            return res.status(400).json({ msg: 'Salle is not available for the selected timeslot' });
        }

        const newReservation = new Reservation({
            user: req.user.id,
            salle,
            start_time: moment(start_time).toDate(),
            end_time: moment(end_time).toDate()
        });
        await newReservation.save();

        // Ajouter la réservation à la salle
        salleDoc.reservations.push({
            start_time: newReservation.start_time,
            end_time: newReservation.end_time,
            reservation_id: newReservation._id
        });
        await salleDoc.save();

        res.status(201).json(newReservation);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
};
exports.updateReservation = async (req, res) => {
    const { salle, start_time, end_time } = req.body;
    const reservationId = req.params.id;

    try {
        let reservation = await Reservation.findById(reservationId);
        if (!reservation) {
            return res.status(404).json({ msg: 'Reservation not found' });
        }

        // Récupérer l'ancienne salle (avant mise à jour)
        const oldSalleDoc = await Salle.findById(reservation.salle);

        // Vérifier si la nouvelle salle est disponible
        const newSalleDoc = await Salle.findById(salle);
        const isAvailable = newSalleDoc.reservations.every(
            (res) => (res.reservation_id.equals(reservationId) ||
                      moment(res.end_time).isSameOrBefore(moment(start_time)) ||
                      moment(res.start_time).isSameOrAfter(moment(end_time)))
        );

        if (!isAvailable) {
            return res.status(400).json({ msg: 'Salle is not available for the selected timeslot' });
        }

        // Retirer l'ancienne réservation de l'ancienne salle
        if (oldSalleDoc) {
            oldSalleDoc.reservations = oldSalleDoc.reservations.filter(
                (res) => !res.reservation_id.equals(reservation._id)
            );
            await oldSalleDoc.save();
        }

        // Mettre à jour la réservation
        reservation.salle = salle;
        reservation.start_time = moment(start_time).toDate();
        reservation.end_time = moment(end_time).toDate();
        await reservation.save();

        // Ajouter la nouvelle réservation à la nouvelle salle
        newSalleDoc.reservations.push({
            start_time: reservation.start_time,
            end_time: reservation.end_time,
            reservation_id: reservation._id
        });
        await newSalleDoc.save();

        res.json(reservation);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
};
exports.deleteReservation = async (req, res) => {
    try {
        const reservationId = req.params.id;
        const reservation = await Reservation.findById(reservationId);

        if (!reservation) {
            return res.status(404).json({ msg: 'Reservation not found' });
        }

        // Retirer la réservation de la salle
        const salleDoc = await Salle.findById(reservation.salle);
        if (salleDoc) {
            salleDoc.reservations = salleDoc.reservations.filter(
                (res) => !res.reservation_id.equals(reservation._id)
            );
            await salleDoc.save();
        }

        // Supprimer la réservation
        await Reservation.deleteOne({ _id: reservationId });
        res.json({ msg: 'Reservation removed' });
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
};
exports.getUserReservations = async (req, res) => {
    try {
        const reservations = await Reservation.find({ user: req.user.id }).populate('salle');
        res.json(reservations);
    } catch (err) {
        console.error(err.message);
        res.status(500).send('Server error');
    }
};

